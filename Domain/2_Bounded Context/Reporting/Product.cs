﻿using System;
using SmartStepGroup.AgileShop.Domain.BoundedContext.Sale;

namespace SmartStepGroup.AgileShop.Domain.BoundedContext.Reporting
{
    public class Product
    {
        public string Name { get; }
        public DateTime Date { get; }
        public Money TotalSalesCost { get; }
        public Money TotalSupplyCost { get; }
    }
}