using System.Collections.Generic;

namespace SmartStepGroup.AgileShop.Domain.BoundedContext.Sale
{
    public class Product {
		public string Name  { get; }
        public IList<ProductVariation> Variations { get; }

        public Product(string name) {
			Name = name;
            Variations = new List<ProductVariation>(); 
        }

		public Product(string name, Money price) {
			Name = name;
            Variations = new List<ProductVariation> {
                new ProductVariation(Name, price)
            }; 
		}
    }
}

