namespace SmartStepGroup.AgileShop.Domain.BoundedContext.Sale
{
	public class Discount {
		public int MinCount { get; set; }
		public int MaxCount { get; set; }
		public decimal Percent { get; set; }
	}
}