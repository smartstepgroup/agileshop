using System.Collections.Generic;
using System.Linq;

namespace SmartStepGroup.AgileShop.Domain.BoundedContext.Sale
{
    public class ProductVariation {
        public string Name { get; }
        public Money Price { get; }
        public Quantity Quantity { get; }
        private List<Discount> Discounts { get; }


        public ProductVariation(string name, Money price) {
            Name = name;
            Price = price;
            Discounts = new List<Discount>();
        }

        public Money DiscountedPrice
        {
            get { return Price * (1 - DiscountPercent / 100); }
        }

        private decimal DiscountPercent
        {
            get
            {
                var discount = Discounts.FirstOrDefault(_ => _.MinCount <= Quantity && Quantity < _.MaxCount);
                if (discount != null)
                {
                    return discount.Percent;
                }
                return 0;
            }
        }
    }
}