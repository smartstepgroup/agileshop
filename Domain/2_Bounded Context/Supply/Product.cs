﻿using System;
using SmartStepGroup.AgileShop.Domain.BoundedContext.Sale;

namespace SmartStepGroup.AgileShop.Domain.BoundedContext.Supply
{
    public class Product
    {
        public Supplier Supplier { get; }
        public string ConsignmentNumber { get; }
        public string SupplierReferenceNumber { get; }
        public Money PurchasePrice { get; }
        public DateTime SupplyDateTime { get; }
        public DateTime UseByDate { get; }
    }
}