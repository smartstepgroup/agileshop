﻿namespace SmartStepGroup.AgileShop.Domain.BoundedContext.Manufacturing
{
    public class Ingredient
    {
        public Material Material { get; }
        public uint Quantity { get; }
    }
}