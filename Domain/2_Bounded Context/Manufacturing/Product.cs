﻿using System.Collections.Generic;

namespace SmartStepGroup.AgileShop.Domain.BoundedContext.Manufacturing
{
    public class Product
    {
        public IEnumerable<Ingredient> Ingredients { get; }
    }
}