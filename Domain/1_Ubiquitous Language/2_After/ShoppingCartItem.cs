﻿namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.After {
	public class ShoppingCartItem {
        public ShoppingCartItem(ProductVariation productVariation) {
            ProductVariation = productVariation;
            Quantity = new Quantity(1);
        }
        
		public ProductVariation ProductVariation { get; private set; }

		public Quantity Quantity { get; private set; }
        
        public Money DiscountedPrice { 
            get {
                return ProductVariation.GetDiscountedPrice(Quantity);
            }
        }
        
        public Money SavingPerItem {
            get {
                return ProductVariation.Price - DiscountedPrice;
            }
        }
        
		public decimal Cost {
			get {
				return DiscountedPrice * Quantity;
			}
		}
	}
}

