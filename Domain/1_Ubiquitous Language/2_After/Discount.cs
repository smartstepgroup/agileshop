namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.After {
	public class Discount {
		public Quantity MinQuantity { get; set; }
		public Quantity MaxQuantity { get; set; }
		public decimal Percent { get; set; }

	    public bool IsApplicable(Quantity quantity) {
	        return MinQuantity <= quantity && quantity < MaxQuantity;
	    }
	}
}