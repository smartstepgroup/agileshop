namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.After
{
	public enum UnitOfMeasure
	{
		Piece,
		Milliliter,
		Gram
	}
}

