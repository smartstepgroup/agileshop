namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.After {
    public class ColorProductVariation : ProductVariation {
        public Color Color { get; }

        public ColorProductVariation(Color color, Money price) : base (color.ToString(), price) {
            Color = color;
        }
    }
}