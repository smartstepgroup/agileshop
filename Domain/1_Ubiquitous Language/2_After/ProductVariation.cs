using System.Collections.Generic;
using System.Linq;

namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.After {
    public class ProductVariation {
        public string Name { get; }
        public Money Price { get; }
		private List<Discount> Discounts { get; }
        
        public ProductVariation(string name, Money price) {
            Name = name;
            Price = price;
			Discounts = new List<Discount>();
        }

        public Money GetDiscountedPrice(Quantity quantity) {
			var discount = GetDiscountPercent(quantity);
			return Price * (1 - discount / 100);
		}

		private decimal GetDiscountPercent(Quantity quantity) {
			var discount = Discounts.SingleOrDefault(_ => _.IsApplicable(quantity));
			if (discount != null) {
				return discount.Percent;
			}
			return 0;
		}
    }
}