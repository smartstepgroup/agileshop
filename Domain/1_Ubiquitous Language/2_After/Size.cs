namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.After {
    public class Size {
        public Size(decimal value, UnitOfMeasure unitOfMeasure) {
            Value = value;
            UnitOfMeasure = unitOfMeasure;
        }
        
        public decimal Value { get; }
        public UnitOfMeasure UnitOfMeasure { get; }

        #region Equality
        protected bool Equals(Size other) {
            return Value == other.Value && UnitOfMeasure == other.UnitOfMeasure;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Size) obj);
        }

        public override int GetHashCode() {
            unchecked
            {
                return (Value.GetHashCode() * 397) ^ (int) UnitOfMeasure;
            }
        }

        public static bool operator ==(Size left, Size right) {
            return Equals(left, right);
        }

        public static bool operator !=(Size left, Size right) {
            return !Equals(left, right);
        }

        #endregion
    }
    
    public static class SizeExtensions {
        public static Size ml(this int value) {
            return new Size(value, UnitOfMeasure.Milliliter); 
        }

        public static Size l(this double value) {
            return new Size((decimal)value * 1000, UnitOfMeasure.Milliliter); 
        }
    }
}