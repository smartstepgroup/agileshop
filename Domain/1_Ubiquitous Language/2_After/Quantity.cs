namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.After {
public class Quantity {
        private uint value;
        
        public Quantity(uint value) {
            this.value = value;
        }
        
        public void Increase() {
            value++;
        }
        
        public void Decrease() {
            if (value > 0) value--;
        }
        
        public static implicit operator uint(Quantity quantity) {
            return quantity.value;
        }
    }
}