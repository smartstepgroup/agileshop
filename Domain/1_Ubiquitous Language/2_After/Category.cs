namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.After
{
    public enum Category {
        Scrum,
        Kanban,
        Toys,
        Artifacts,
        Dangerous
    }
}