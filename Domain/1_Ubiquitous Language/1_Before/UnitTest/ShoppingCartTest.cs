﻿#region Usings

using System.Collections.Generic;
// using NUnit.Framework;

#endregion

namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.Before.UnitTest
{
//    [TestFixture]
    public class ShoppingCartTest
    {
//        [Test]
        public void CanBuyProductsWithDiffentSizeAndColor() {
            var kanbanHookah = new ShoppingCartItem
            {
                Count = 1,
                Product = new Product("Канбан кальян", 100)
            };
            var kentBeckTears = new ShoppingCartItem
            {
                Count = 1,
                Product = new Product("Слезы Кента Бека", 12.99m)
                {
                    AvailableSizes = new List<decimal> { 0.125m, 0.5m, 1.5m }
                }
            };
            var agileBeads = new ShoppingCartItem
            {
                Count = 1,
                Product = new Product("Аджальные шарики", 15.99m)
                {
                    AvailableColors = new List<Color> { Color.Green, Color.Red}
                }
            };

            var shoppingCart = new ShoppingCart();
            shoppingCart.Items.Add(kanbanHookah);
            shoppingCart.Items.Add(kentBeckTears);
            shoppingCart.Items.Add(agileBeads);
            shoppingCart.Items[1].Count = 3;

//            Assert.That(shoppingCart.TotalCost, Is.EqualTo(100 + 12.99*3 + 15.99));
        }
    }
}