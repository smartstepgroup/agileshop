namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.Before
{
    public enum Category {
        Scrum,
        Kanban,
        Toys,
        Artifacts,
        Dangerous
    }
}