namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.Before
{
	public enum UnitOfMeasure
	{
		Piece,
		Milliliter,
		Gram
	}
}

