﻿using System.Collections.Generic;
using System.Linq;

namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.Before {
	public class ShoppingCart {
		public List<ShoppingCartItem> Items { get; set; }
        
        public ShoppingCart() {
            Items = new List<ShoppingCartItem>();
        }

        public void DeleteAllItems() {
            Items.Clear();
        }
        
		public decimal TotalCost {
			get {
				return Items.Sum(_ => _.Sum);
			}
		}
	}
}

