﻿namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.Before {
	public class ShoppingCartItem {
		public Product Product { get; set; }

		public int Count { get; set; }

		public decimal Sum {
			get {
				return Product.GetDiscountedPrice(Count) * Count;
			}
		}
        
        public decimal DiscountPercent {
            get {
                return Product.GetDiscountPercent(Count);
            }
        }
	}
}

