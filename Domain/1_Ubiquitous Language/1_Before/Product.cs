using System.Collections.Generic;
using System.Linq;

namespace SmartStepGroup.AgileShop.Domain.UbiquitousLanguage.Before {
	public class Product {
		public string Name  { get; private set; }
		public decimal Price { get; private set; }
		public List<Category> Categories { get; private set; }
		public List<Color> AvailableColors { get; set; }
		public List<decimal> AvailableSizes { get; set; }
		public List<Material> Materials { get; set; }
		public List<UnitOfMeasure> UnitOfMeasures {	get; set; }
		public List<Discount> Discounts { get; set; }

		public Product(string name, decimal price) {
			Name = name;
			Price = price;

            Categories = new List<Category>();
			AvailableColors = new List<Color> { Color.NoColor };
            AvailableSizes = new List<decimal>();
			Materials = new List<Material>();
			UnitOfMeasures = new List<UnitOfMeasure>();
			Discounts = new List<Discount>();
		}

		public decimal GetDiscountedPrice(int number) {
			var discount = GetDiscountPercent(number);
			return Price * (1 - discount / 100);
		}

		public decimal GetDiscountPercent(int number) {
			var discount = Discounts.FirstOrDefault(_ => _.MinCount <= number && number < _.MaxCount);
			if (discount != null) {
				return discount.Percent;
			}
			return 0;
		}
	}
}

